/* vim: ft=c ff=unix fenc=utf-8
 * file: opts_func.h
 */
#ifndef _OPTS_FUNC_1363000339_H_
#define _OPTS_FUNC_1363000339_H_
#include <db.h>

#define _OPTS_FSZ 32
#define _OPTS_UA_SZ 256
#define _OPTS_MAXPATH 1024
#define _OPTS_MAXSSHKEY 2048

struct opts_t
{
	/* auth */
	char dealer[_OPTS_FSZ];
	char dealerkey[_OPTS_FSZ];
	char username[_OPTS_FSZ];
	char password[_OPTS_FSZ];
	char useragent[_OPTS_UA_SZ];
	/* ssh */
	char sshkey[_OPTS_MAXPATH];
	char sshkey_data[_OPTS_MAXSSHKEY];
	size_t sshkey_size;
	DB *db; /* list user -> sshkey */
	DB *dbsys; /* cookie/lasuser info */
};

enum _opts_set_t
{
	_OPTS_SETDEALER = 1,
	_OPTS_SETDEALKEY,
	_OPTS_SETUSERNAME,
	_OPTS_SETPASSWORD,
	_OPTS_SETUA,
	_OPTS_SETSSHKEY,
	_OPTS_SETDB
};

extern struct opts_list_t opts_list[];

void optsf_term (struct opts_t *opts);

#endif /* _OPTS_FUNC_1363000339_H_ */

