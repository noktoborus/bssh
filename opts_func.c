/* vim: ft=c ff=unix fenc=utf-8
 * file: opts_func.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "opts.h"
#include "opts_func.h"

void
optsf_term (struct opts_t *opts)
{
	if (opts->db)
		opts->db->close (opts->db, 0);
	if (opts->dbsys)
		opts->dbsys->close (opts->dbsys, 0);
}

void *
_opts_print (struct opts_t *opts, void *cont_data, uint8_t id, size_t argc, char **argv)
{
	printf ("Dealer: %s\n", opts->dealer);
	printf ("Delaer Key: %s\n", opts->dealerkey);
	printf ("Username: %s\n", opts->username);
	printf ("Password: %s\n", opts->password);
	printf ("User-Agent: %s\n", opts->useragent);
	return NULL;
}

void *
_opts_set (struct opts_t *opts, void *cont_data, uint8_t id, size_t argc, char **argv)
{
	switch (id)
	{
		case _OPTS_SETDEALER:
			strncpy (opts->dealer, argv[0], _OPTS_FSZ);
			break;
		case _OPTS_SETDEALKEY:
			strncpy (opts->dealerkey, argv[0], _OPTS_FSZ);
			break;
		case _OPTS_SETUSERNAME:
			strncpy (opts->username, argv[0], _OPTS_FSZ);
			break;
		case _OPTS_SETPASSWORD:
			strncpy (opts->password, argv[0], _OPTS_FSZ);
			break;
		case _OPTS_SETUA:
			strncpy (opts->useragent, argv[0], _OPTS_UA_SZ);
			break;
		case _OPTS_SETSSHKEY:
			{
				int fd;
				size_t ok;
				if ((fd = open (argv[0], O_RDONLY)) != -1)
				{
					/* TODO: check file size */
					if ((ok = read (fd, (void*)opts->sshkey_data, _OPTS_MAXSSHKEY - 1)) > 0)
					{
						opts->sshkey_data[ok] = '\0';
						opts->sshkey_size = (size_t)ok;
						strncpy (opts->sshkey, argv[0], _OPTS_MAXPATH);
					}
					else
						perror ("set_sshkey");
					close (fd);
				}
				else
					perror ("set_sshkey");
				break;
			}
		case _OPTS_SETDB:
			{
				int ret;
				char path[MAXPATH];
				/* open keys db */
				if (db_create (&opts->db, NULL, 0))
					fprintf (stderr, "set_db: can't initialize DB (keys.db)\n");
				else
				{
					snprintf (path, MAXPATH, "%s/keys.db", argv[0]);
					ret = opts->db->open (opts->db, NULL, path, NULL, DB_BTREE, DB_CREATE, 0);
					if (ret)
					{
						fprintf (stderr, "set_db: can't open or create file: '%s', code: %d\n",\
							path, ret);
						opts->db->close (opts->db, 0);
						opts->db = NULL;
					}
				}
				/* open dbsys */
				if (db_create (&opts->dbsys, NULL, 0))
					fprintf (stderr, "set_db: can't initialize DB (system.db)\n");
				else
				{
					snprintf (path, MAXPATH, "%s/system.db", argv[0]);
					ret = opts->dbsys->open (opts->dbsys, NULL, path, NULL, DB_BTREE, DB_CREATE, 0);
					if (ret)
					{
						fprintf (stderr, "set_db: can't open or create file: '%s', code: %d\n",\
							path, ret);
						opts->dbsys->close (opts->dbsys, 0);
						opts->dbsys = NULL;
					}
				}
			}
			break;
	}
	return NULL;
}

void *
_opts_source (void *opts_data, void *cont_data, uint8_t id, size_t argc, char **argv)
{
	opts_parse_file (opts_data, opts_list, argv[0]);
	return NULL;
}

struct opts_list_t opts_list[] =
{
	/* auth */
	{"set_ua", _OPTS_SETUA, 0, (opts_callback_t)_opts_set, 1, NULL},
	{"set_dealer", _OPTS_SETDEALER, 0, (opts_callback_t)_opts_set, 1, NULL},
	{"set_dealerkey", _OPTS_SETDEALKEY, 0, (opts_callback_t)_opts_set, 1, NULL},
	{"set_username", _OPTS_SETUSERNAME, 0, (opts_callback_t)_opts_set, 1, NULL},
	{"set_password", _OPTS_SETPASSWORD, 0, (opts_callback_t)_opts_set, 1, NULL},
	{"set_sshkey", _OPTS_SETSSHKEY, 0, (opts_callback_t)_opts_set, 1, NULL},
	{"set_db", _OPTS_SETDB, 0, (opts_callback_t)_opts_set, 1, NULL},
	/* misc */
	{"source", 0, 0, _opts_source, 1, NULL},
	{"print", 0, 0, (opts_callback_t)_opts_print, 0, NULL},
	{"", 0, 0, NULL, 0, NULL}
};

