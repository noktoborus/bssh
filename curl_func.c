/* vim: ft=c ff=unix fenc=utf-8
 * file: curl_opts.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <curl/curl.h>

#include "opts.h"
#include "curl_func.h"

/*
 * Login (get cookies):
 * http://hp.beget.ru/?login=1
 * username: $dealer
 * password: $dealerpassord
 * worker: $login
 * workpasswd: $password
 * Result ->
 * Cookies
 *
 * Search:
 * http://hp.beget.ru/main?ajax&method=ajax_search_client3
 * p0: noktq
 * p1: 1  // equal
 * p2: -1 //
 * p3: -1 //
 * p4: -1 //
 * p5: 0  //
 * p6: none // sort
 * p7: 10 // results count
 * p8: 0  // search: domain
 * p9: 0  // search: email
 * p10: 0 // search: vat
 * p11: 0
 * p12: 0
 * Result ->
 * 40449|noktq||Noble|-|-|terra|2012-11-14|N|-|noktoborus@gmail.com|-|-|0|0|
 * id|username|family name|tariff|-|-|server|register date|blocked|-|email|-|-|year discount|deleted|
 *
 * Get info:
 * // id in Url: ...customerinfo{$id}?...
 * http://hp.beget.ru/customerinfo40449?ajax&method=ajax_load_account_inf
 * Result ->
 * 319839|0.00|253|253|2100-00-00|N|2|Y|/bin/bash|0|50|0|0|0.00|0|
 * 319839|0.00|253|253|2013-03-07|Y|2|Y|/bin/bash|0|50|0|0|0.00|0
 * password|debt|freedays|freedays|blok|tariff|ssh|shell|no-interest*
 *
 * Get small info:
 * http://hp.beget.ru/main?ajax&method=ajax_get_status
 * p0: 40449 // id
 * p1: -
 * p2: -
 * p3: -
 * p4: -
 * Result ->
 * 40449|Y|236|0.00|0|50|
 * id|ssh|days|balans|cp use (7)|cp limit
 *
 * Generate password:
 * // id in Url: ...customerinfo{$id}?...
 * http://hp.beget.ru/customerinfo40449?ajax&method=ajax_generate_passwd
 * Result ->
 * 0
 *
 * SSH:
 * // id in Url: ...customerinfo{$id}?...
 * http://hp.beget.ru/customerinfo40449?ajax&method=ajax_change_ssh
 * p0: N // Off
 * p0: Y // On
 *
 * Blocking:
 * // id in Url: ...customerinfo{$id}?...
 * http://hp.beget.ru/customerinfo40449?ajax&method=ajax_change_account_status
 * p0: Y // On
 * p1: 0 // send email
 * p2: test // comment
 * p3: 0 // block mobile number
 * --
 * p0: N // Off
 * p1: false
 * p2: false
 * p3: 0
 *
 */

/* lib */
static char *
_list2string (struct curl_slist *cookie)
{
	char *out = NULL;
	char *tut = NULL;
	size_t len = 0u;
	size_t ten = 0u;
	while (cookie)
	{
		/* printf ("# %s\n", cookie->data); */
		ten = strlen (cookie->data);
		/* realloc data */
		tut = realloc (out, len + ten + 2);
		if (tut)
			out = tut;
		else
			break;
		strcpy (&(out[len]), cookie->data);
		out[len += ten] = '\n';
		out[++len] = '\0';
		cookie = cookie->next;
	}
	if (!tut && out)
	{
		free (out);
		return NULL;
	}
	return out;
}

static size_t
_line2array (char *line, size_t argc, char **argv)
{
	size_t no = 0;
	size_t curn = 1; /* include first column */
	size_t len = 0;
	if (argc == 0)
		return 0u;
	/* calc len */
	while (line[len] != '\0' && line[len] != '\n')
	{
		if (line[len] == '|')
			curn++;
		len ++;
	}
	if (!len)
		return 0u;
	/* fill array */
	for (curn = 1, argv[0] = line, no = 0; no < len && curn < argc; no++)
	{
		if (line[no] == '|')
		{
			line[no] = '\0';
			argv[curn++] = &line[no + 1];
		}
	}
	/* update counter for append NEXT data */
	if (curn >= argc)
	{
		curn = argc - 1;
	}
	/* finalize string */
	if (line[len] == '\n')
	{
		line[len] = '\0';
		/* set pointer to EOL */
		argv[curn] = &line[len + 1];
	}
	else
	{
		/* set end pointer to \0 */
		argv[curn] = &line[len];
	}
	return curn;
}

void
_curl_buffer_free (struct _curl_buffer_t *buffer, bool f)
{
	if (buffer)
	{
		if (f && buffer->data)
			free (buffer->data);
		memset (buffer, 0, sizeof (struct _curl_buffer_t));
	}
}


static size_t
_curl_zeroput (void *ptr, size_t size, size_t nmemb, void *userdata)
{
	return (size * nmemb);
}

static size_t
_curl_memget (void *ptr, size_t size, size_t nmemb, struct _curl_buffer_t *buffer)
{
	size_t isz = nmemb * size;
	if (!isz || buffer->up >= buffer->sz)
	{
		buffer->up = 0;
		return 0;
	}
	/* */
	if (buffer->sz - buffer->up < isz)
	{
		isz = buffer->sz - buffer->up;
		memcpy (ptr, (const void *)&buffer->data[buffer->up], isz);
		buffer->up = buffer->sz;
		return isz;
	}
	else
	{
		memcpy (ptr, (const void *)&buffer->data[buffer->up], isz);
		buffer->up += isz;
		return isz;
	}
	return 0;
}

static size_t
_curl_memput (void *ptr, size_t size, size_t nmemb, struct _curl_buffer_t *buffer)
{
	size_t isz = nmemb * size;
	void *tmp;
	if (!buffer)
		return 0u;
	if (nmemb && size)
	{
		/* realloc */
		tmp = realloc ((void *)buffer->data, buffer->sz + isz + 1);
		if (tmp)
		{
			buffer->data = (char *)tmp;
			memcpy ((void *)(buffer->data + buffer->sz), (const void *)ptr,
					isz);
			buffer->sz += isz;
			buffer->data[buffer->sz] = '\0';
		}
	}
	return isz;
}

long
_curl_query (struct curl_opts_t *opts, struct _curl_buffer_t *buffer, char **_cookie, const char *url, const char *fields, uint8_t flags)
{
	long ret;
	struct curl_slist *cookie;
	CURL *curl = curl_easy_init ();
	curl_easy_setopt (curl, CURLOPT_URL, url);
	curl_easy_setopt (curl, CURLOPT_POST, 1);
	curl_easy_setopt (curl, CURLOPT_POSTFIELDS, fields);
	curl_easy_setopt (curl, CURLOPT_USERAGENT, opts->opts.useragent);
	if (flags & _FLAG_VERB)
		curl_easy_setopt (curl, CURLOPT_VERBOSE, 1);
	/* init cookie engine */
	if (_cookie)
		curl_easy_setopt (curl, CURLOPT_COOKIELIST, *_cookie ? *_cookie : "");
	if (flags & _FLAG_DATA)
	{
		curl_easy_setopt (curl, CURLOPT_WRITEDATA, buffer);
		curl_easy_setopt (curl, CURLOPT_WRITEFUNCTION, _curl_memput);
		/* free current data */
	}
	else
	{
		curl_easy_setopt (curl, CURLOPT_WRITEFUNCTION, _curl_zeroput);
	}
	/* get data */
	curl_easy_perform (curl);
	/* end */
	curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &ret);
	if (flags & _FLAG_COOK)
	{
		if (_cookie)
		{
			if (*_cookie)
			{
				free (*_cookie);
				*_cookie = NULL;
			}
			curl_easy_getinfo (curl, CURLINFO_COOKIELIST, &cookie);
			if (cookie)
			{
				*_cookie = _list2string (cookie);
				curl_slist_free_all (cookie);
			}
		}
	}
	curl_easy_cleanup (curl);
	return ret;
}

long
_curl_ftpget (struct curl_opts_t *opts, struct _curl_buffer_t *buffer, const char *url, uint8_t flags)
{
	long ret;
	CURL *curl = curl_easy_init ();
	/* configure */
	curl_easy_setopt (curl, CURLOPT_URL, url);
	curl_easy_setopt (curl, CURLOPT_USERNAME, opts->u.name);
	curl_easy_setopt (curl, CURLOPT_PASSWORD, opts->u.password);
	curl_easy_setopt (curl, CURLOPT_WRITEDATA, buffer);
	curl_easy_setopt (curl, CURLOPT_WRITEFUNCTION, _curl_memput);
	if (flags & _FLAG_VERB)
		curl_easy_setopt (curl, CURLOPT_VERBOSE, 1);
	/* process */
	ret = curl_easy_perform (curl);
	/* cleanup */
	curl_easy_cleanup (curl);
	return ret;
}

long
_curl_ftpput_authorized (struct curl_opts_t *opts, struct _curl_buffer_t *buffer, uint8_t flags)
{
	long ret;
	char url[1024];
	struct curl_slist *postputcmd = NULL;
	CURL *curl = curl_easy_init ();
	/* configure */
	snprintf (url, 1024, "ftp://%s.beget.ru/.ssh/.authorized_upload", opts->u.server);
	postputcmd = curl_slist_append (postputcmd, "RNFR .authorized_upload");
	postputcmd = curl_slist_append (postputcmd, "RNTO authorized_keys");
	postputcmd = curl_slist_append (postputcmd, "SITE CHMOD 0600 authorized_keys");
	postputcmd = curl_slist_append (postputcmd, "CWD /");
	postputcmd = curl_slist_append (postputcmd, "SITE CHMOD 0700 .ssh");
	curl_easy_setopt (curl, CURLOPT_FTP_CREATE_MISSING_DIRS, 1l);
	curl_easy_setopt (curl, CURLOPT_USERNAME, opts->u.name);
	curl_easy_setopt (curl, CURLOPT_PASSWORD, opts->u.password);
	curl_easy_setopt (curl, CURLOPT_URL, url);
	curl_easy_setopt (curl, CURLOPT_UPLOAD, 1l);
	curl_easy_setopt (curl, CURLOPT_READFUNCTION, _curl_memget);
	curl_easy_setopt (curl, CURLOPT_READDATA, buffer);
	curl_easy_setopt (curl, CURLOPT_POSTQUOTE, postputcmd);
	if (flags & _FLAG_VERB)
		curl_easy_setopt (curl, CURLOPT_VERBOSE, 1);
	/* run */
	ret = curl_easy_perform (curl);
	/* cleanup */
	curl_slist_free_all (postputcmd);
	curl_easy_cleanup (curl);
	return ret;
}

static bool
_auth (struct curl_opts_t *opts, bool noupdate)
{
	long ret;
	char fields[1024];
	if (opts->cookie)
	{
		/* check login: if code != 200, then exeption */
		ret = _curl_query (opts, NULL, &opts->cookie, "http://hp.beget.ru/main.php", "", 0);
		if (ret == 200)
			return true;
	}
	if (!noupdate)
	{
		snprintf (fields, 1024, "username=%s&password=%s&worker=%s&workpasswd=%s",
				opts->opts.dealer, opts->opts.dealerkey,
				opts->opts.username, opts->opts.password);
		_curl_query (opts, NULL, &opts->cookie, "http://hp.beget.ru/?login=1", fields, _FLAG_COOK);
		/* check again */
		if (_auth (opts, true))
			return true;
		/* print error */
		fprintf (stderr, "auth: check failed\n");
	}
	if (opts->cookie)
	{
		free (opts->cookie);
		opts->cookie = NULL;
	}
	return false;
}

void
curlf_init (struct curl_opts_t *opts)
{
	/* init curl */
	curl_global_init (CURL_GLOBAL_ALL);
	/* load last data */
	if (opts->opts.dbsys)
	{
		DBT keyval[2];
		struct _db_store_t *dbs;
		memset ((void *)keyval, 0, sizeof (DBT) * 2);
		keyval[0].data = "lastuser";
		keyval[0].size = 9; /* sizeof ("lastuser") */
		if (!opts->opts.dbsys->get (opts->opts.dbsys, NULL, keyval, keyval + 1, 0))
		{
			if (keyval[1].size != sizeof (struct _db_store_t))
			{
				fprintf (stderr, "init: store value size not expect: struct's %lu != %u\n",
					sizeof (struct _db_store_t), keyval[1].size);
			}
			else
			{
				dbs = keyval[1].data;
				memcpy ((void *)opts->u.id, (const void *)dbs->id, _CURL_SZ);
				memcpy ((void *)opts->u.name, (const void *)dbs->user, _CURL_SZ);
				memcpy ((void *)opts->u.password, (const void *)dbs->password, _CURL_SZ);
				memcpy ((void *)opts->u.server, (const void *)dbs->server, _CURL_SZ);
				opts->u.flags = dbs->flags;
			}
		}
		/* load cookies */
		keyval[0].data = "cookie";
		keyval[0].size = 7; /* sizeof ("cookie") */
		if (!opts->opts.dbsys->get (opts->opts.dbsys, NULL, keyval, keyval + 1, 0))
		{
			opts->cookie = strndup ((char *)keyval[1].data, keyval[1].size);
			if (!opts->cookie)
				fprintf (stderr, "init: can't load cookies, because mem\n");
		}
	}
}

void
curlf_term (struct curl_opts_t *opts)
{
	curl_global_cleanup ();
	/* store last data */
	if (opts->opts.dbsys)
	{
		DBT keyval[2];
		struct _db_store_t dbs;
		memset ((void *)keyval, 0, sizeof (DBT) * 2);
		memset ((void *)&dbs, 0, sizeof (struct _db_store_t));
		keyval[0].data = "lastuser";
		keyval[0].size = 9; /* sizeof ("lastuser") */
		memcpy (dbs.id, opts->u.id, _CURL_SZ);
		memcpy (dbs.user, opts->u.name, _CURL_SZ);
		memcpy (dbs.server, opts->u.server, _CURL_SZ);
		memcpy (dbs.password, opts->u.password, _CURL_SZ);
		dbs.flags = opts->u.flags;
		keyval[1].data = &dbs;
		keyval[1].size = sizeof (struct _db_store_t);
		opts->opts.dbsys->put (opts->opts.dbsys, NULL, keyval, keyval + 1, 0);
		/* save cookies */
		if (opts->cookie)
		{
			keyval[0].data = "cookie";
			keyval[0].size = 7; /* sizeof ("cookie") */
			keyval[1].data = opts->cookie;
			keyval[1].size = strlen (opts->cookie) + 1; /* with \0 */
			opts->opts.dbsys->put (opts->opts.dbsys, NULL, keyval, keyval + 1, 0);
		}
		if (opts->u.cookie)
		{
			keyval[0].data = "lastcookie";
			keyval[0].size = 11;
			keyval[1].data = opts->u.cookie;
			keyval[1].size = strlen (opts->cookie) + 1;
			opts->opts.dbsys->put (opts->opts.dbsys, NULL, keyval, keyval + 1, 0);
		}
	}
	/*  */
	if (opts->cookie)
		free (opts->cookie);
	if (opts->u.cookie)
		free (opts->u.cookie);
	optsf_term (&opts->opts);
}

/*** hp funcs */
/* update keys on server */
static void
__ssh_update_keys (struct curl_opts_t *opts, uint8_t flags, struct _curl_buffer_t *buffer, char *sshkey_data, size_t sshkey_size)
{
	char url[1024];
	long ret;
	_curl_buffer_free (buffer, true);
	/* prevent exception */
	flags &= ~(_UPDATE_KEYPERM | _UPDATE_KEYNONE);
	/* DO NOT CROSS THE STREAMS! */
	if ((flags & (_UPDATE_KEYADD | _UPDATE_KEYREM)) == (_UPDATE_KEYADD | _UPDATE_KEYREM)\
		|| !flags)
		return;
	snprintf (url, 1024, "ftp://%s.beget.ru/.ssh/authorized_keys",\
		opts->u.server);
	ret = _curl_ftpget (opts, buffer, url, 0);
	if (ret != CURLE_REMOTE_FILE_NOT_FOUND && ret != CURLE_REMOTE_ACCESS_DENIED && ret != CURLE_OK)
	{
		fprintf (stderr, "ssh: get authorized_keys info failed -> [%ld] %s\n",\
			ret, curl_easy_strerror (ret));
		return;
	}
	/* if no data -> file not exists or not allowed to read
	* simple append or none (when write)
	*/
	if (!buffer->data || !buffer->data[0])
	{
		/* if open */
		if (flags & _UPDATE_KEYADD)
		{
			buffer->data = strdup (sshkey_data);
			buffer->sz = sshkey_size;
			/* set perms for write */
			flags |= _UPDATE_KEYPERM;
		}
		else /* if close */
		if (flags & _UPDATE_KEYREM)
		{
			/* remove empty keys from db */
			flags |= _UPDATE_KEYNONE;
		}
	}
	/* else search key in list, if not exists -> append or remove */
	else
	{
		char *pos_begin;
		char *pos_end;
		size_t len;
		/* if key already in file */
		if ((pos_begin = strstr (buffer->data, sshkey_data)))
		{
			/* if close */
			if (flags & _UPDATE_KEYREM)
			{
				/* search end */
				if (!(pos_end = strchr (pos_begin, '\n')))
				{
					/* if end not found - simple trim */
					*pos_begin = '\0';
					buffer->sz = pos_begin - buffer->data;
				}
				else
				{
					pos_end++; /* increment to '\n' */
					len = buffer->sz - (pos_begin - buffer->data) - (pos_end - pos_begin);
					/* copy with '\0' */
					memmove ((void *)pos_begin, (const void *)pos_end, len);
					/* buffer->sz -= (pos_end - pos_begin); */
					buffer->sz = pos_begin - buffer->data + len;
				}
				buffer->data[buffer->sz] = '\0';
				/* flag for upload */
				flags |= _UPDATE_KEYPERM;
			}
			else
			if (flags & _UPDATE_KEYADD)
			{
				/* simple update db */
				flags |= _UPDATE_KEYNONE;
			}
		}
		else
		{
			/* if open then resize and append */
			if (flags & _UPDATE_KEYADD)
			{
				/* realloc with len 2 for '\n' and '\0' */
				pos_end = realloc (buffer->data, buffer->sz + sshkey_size + 2);
				if (!pos_end)
				{
					perror ("ssh");
				}
				else
				{
					buffer->data = pos_end;
					if (buffer->data[buffer->sz - 1] == '\n')
					{
						strncpy (&buffer->data[buffer->sz], sshkey_data, sshkey_size);
						buffer->sz += sshkey_size;
					}
					else
					{
						strncpy (&buffer->data[buffer->sz + 1], sshkey_data, sshkey_size);
						buffer->data[buffer->sz] = '\n';
						buffer->sz += sshkey_size + 1;
					}
					buffer->data[buffer->sz] = '\0';
					/* permit upload */
					flags |= _UPDATE_KEYPERM;
				}
			}
			else
			/* if key not exists in list -> simple remove from db */
			if (flags & _UPDATE_KEYREM)
			{
				flags |= _UPDATE_KEYNONE;
			}
		}
	}
	/* if ready to upload */
	if (flags & _UPDATE_KEYPERM || flags & _UPDATE_KEYNONE)
	{
		long ret;
		if (flags & _UPDATE_KEYPERM)
		{
			ret = _curl_ftpput_authorized (opts, buffer, 0);
			if (ret != CURLE_OK)
			{
				fprintf (stderr, "ssh: upload keys failed -> [%ld] %s\n",\
					ret, curl_easy_strerror (ret));
			}
		}
		if (ret == CURLE_OK || flags & _UPDATE_KEYNONE)
		{
			if (!opts->opts.db)
			{
				fprintf (stderr, "ssh: database not defined\n");
			}
			else
			{
				struct _db_store_t dbs;
				DBT keyval[2];
				char mixkey[_OPTS_MAXSSHKEY];
				/* simple mix */
				memset (mixkey, 0, _OPTS_MAXSSHKEY);
				snprintf (mixkey, _OPTS_MAXSSHKEY, "%s%s", opts->u.id, sshkey_data);
				/* update data */
				memset (keyval, 0, sizeof (DBT) * 2);
				memcpy (dbs.id, opts->u.id, _CURL_SZ);
				memcpy (dbs.user, opts->u.name, _CURL_SZ);
				memcpy (dbs.server, opts->u.server, _CURL_SZ);
				memcpy (dbs.password, opts->u.password, _CURL_SZ);
				memcpy (dbs.sshkey_data, sshkey_data, _OPTS_MAXSSHKEY);
				dbs.flags = opts->u.flags;
				keyval[0].data = mixkey;
				keyval[0].size = _OPTS_MAXSSHKEY;
				if (flags & _UPDATE_KEYADD)
				{
					/* add to db */
					keyval[1].data = &dbs;
					keyval[1].size = sizeof (struct _db_store_t);
					opts->opts.db->put (opts->opts.db, NULL, keyval, keyval + 1, DB_NOOVERWRITE);
				}
				else
				if (flags & _UPDATE_KEYREM)
				{
					/* remove from db */
					opts->opts.db->del (opts->opts.db, NULL, keyval, 0);
				}
			}
		}
	}
}

/* update fields with search result (17 columns) */
static void
__search_update_fields (struct curl_opts_t *opts, char **fields, uint8_t step, struct _curl_buffer_t *buffer)
{
	if (step == 0 || step & _UPDATE_STEP_CLEAR)
	{
		if (opts->u.cookie)
			free (opts->u.cookie);
		memset (&opts->u, 0, sizeof (opts->u));
	}
	/* copy fields from search, 14 column */
	if (step == 0 || step & _UPDATE_STEP_COPYFIELDS)
	{
		/* update info: username, id, server */
		strncpy (opts->u.id, fields[0], _CURL_SZ);
		strncpy (opts->u.name, fields[1], _CURL_SZ);
		strncpy (opts->u.server, fields[6], _CURL_SZ);
		/* set blocked|deleted */
		opts->u.flags = 0;
		if (!strncmp (fields[14], "1", 2))
			opts->u.flags |= _USER_DELETED;
		if (!strncmp (fields[8], "Y", 2))
			opts->u.flags |= _USER_BLOCKED;
	}
	/* update password info */
	if ((step == 0 || step & _UPDATE_STEP_PASSWORD) && !(opts->u.flags & _USER_DELETED))
	{
		char url[1024];
		_curl_buffer_free (buffer, true);
		snprintf (url, 1024, "http://hp.beget.ru/customerinfo%s?ajax&method=ajax_load_account_inf",\
			opts->u.id);
		_curl_query (opts, buffer, &opts->cookie, url, "", _FLAG_DATA);
		if (buffer->data)
		{
			/* only for first field */
			char *rrgv[2];
			size_t rrgc = 0;
			rrgc = _line2array (buffer->data, 2, rrgv);
			if (rrgc)
			{
				strncpy (opts->u.password, rrgv[0], _CURL_SZ);
			}
		}
		/* cleanup */
	}
}

static void *
_search (struct curl_opts_t *opts, struct _curl_buffer_t *obuffer, uint8_t id, size_t argc, char **argv)
{
	struct _curl_buffer_t *buffer;
	/* search <type> <text> [<eq|ne>]
	* eq -- equal search
	* type in [all, email, username, domain, VAT id (ИНН)]
	*/
	char *sopt;
	char fields[1024];
	bool equal = false;
	if (!obuffer)
	{
		buffer = malloc (sizeof (struct _curl_buffer_t));
		_curl_buffer_free (buffer, false);
		/* check args */
		if (argc < 2)
			return OPTS_BADARGS;
		if (argc == 3)
		{
			if (!strncmp (argv[2], "eq", 2))
			{
				equal = true;
			}
			else
			if (strncmp (argv[2], "ap", 2))
				return OPTS_BADARGS;
		}
		if (!strncmp (argv[0], "all", 4))
		{
			sopt = "p8=0&p9=0&p10=0";
		}
		else
		if (!strncmp (argv[0], "domain", 7))
		{
			sopt = "p8=1&p9=0&p10=0";
		}
		else
		if (!strncmp (argv[0], "email", 6))
		{
			sopt = "p8=0&p9=1&p10=0";
		}
		else
		if (!strncmp (argv[0], "vat", 4))
		{
			sopt = "p8=0&p9=0&p10=1";
		}
		else
			return OPTS_BADARGS;
		/* query */
		if (!_auth (opts, false))
			return NULL;
		/* TODO: use variable limit (&p7=) */
		snprintf (fields, 1024,
			"p0=%s&p1=%u&p2=-1&p3=-1&p4=-1&p5=0&p6=none"
			"&p7=10000&%s&p11=0&p12=0", argv[1], (unsigned)equal, sopt);
		_curl_query (opts, buffer, &opts->cookie,
				"http://hp.beget.ru/main?ajax&method=ajax_search_client3",
				fields, _FLAG_DATA);
	}
	else
	{
		buffer = obuffer;
	}
	/* print info */
	if (buffer->data && buffer->up < buffer->sz)
	{
		char *rrgv[17];
		size_t rrgc = 0;
		size_t count = 0;
		rrgv[rrgc] = buffer->data;
		if (!obuffer)
			printf ("%8s | %8s | %12s | %20s | %10s | %10s | %3s | %3s | %40s | name\n"
				"--------------------------------------------------------------------------------"
				"--------------------------------------------------------------------------------\n",
				"no", "ID", "username", "tariff", "server",
				"date", "BLK", "DEL", "email");
		if ((rrgc = _line2array (&buffer->data[buffer->up], 17, rrgv)))
		{
			/* clear fields */
			__search_update_fields (opts, rrgv, _UPDATE_STEP_CLEAR | _UPDATE_STEP_COPYFIELDS, NULL);
			buffer->up = rrgv[rrgc] - buffer->data;
			printf ("%8lu | %8s | %12s | %20s | %10s | %10s | %3s | %3s | %40s | %s\n",
					++count,
					rrgv[0], /* id */
					rrgv[1], /* username */
					rrgv[3], /* tariff */
					rrgv[6], /* server */
					rrgv[7], /* date start */
					(opts->u.flags & _USER_BLOCKED) ? "Y" : "N", /* blocked */
					(opts->u.flags & _USER_DELETED) ? "Y" : "N", /* deleted */
					rrgv[10], /* email */
					rrgv[2] /* name */
				   );
		}
		return (void*)buffer;
	}
	/* cleanup */
	_curl_buffer_free (buffer, true);
	free (buffer);
	return NULL;
}

/***
 * set/get user and id funcs
 */
static bool
__search_userid (struct curl_opts_t *opts, char *field, struct _curl_buffer_t *buffer)
{
	char *rrgv[17]; /* 16 columns + 1 to next */
	size_t rrgc = 17;
	char fields[1024];
	if (_auth (opts, false))
	{
		_curl_buffer_free (buffer, true);
		/* get base info: user/server/blocked */
		snprintf (fields, 1024,
			"p0=%s&p1=1&p2=-1&p3=-1&p4=-1&p5=0&p6=none"
			"&p7=1&p8=0&p9=0&p10=0&p11=0&p12=0",
			field);
		_curl_query (opts, buffer, &opts->cookie,
			"http://hp.beget.ru/main?ajax&method=ajax_search_client3",
			fields, _FLAG_DATA);
		if (buffer->data)
		{
			rrgc = _line2array (buffer->data, rrgc, rrgv);
			/* 16 - count columns */
			if (rrgc == 16)
			{
				__search_update_fields (opts, rrgv, 0, buffer);
				return true;
			}
		}
	}
	return false;
}

static void *
_user (struct curl_opts_t *opts, void *cont_data, uint8_t id, size_t argc, char **argv)
{
	struct _curl_buffer_t buffer;
	_curl_buffer_free (&buffer, false);
	switch (id)
	{
		case _GET:
			if (!opts->u.id[0])
			{
				fprintf (stderr, "user: id not defined\n");
			}
			else
			{
				if (!opts->u.name[0])
					__search_userid (opts, opts->u.id, &buffer);
				printf ("Username: [%s, %s] %s\n",\
					opts->u.flags & _USER_BLOCKED ? "Blocked" : "Active",\
					opts->u.flags & _USER_DELETED ? "Deleted" : "Exists",
					opts->u.name);

			}
			break;
		case _SET:
			/* update info */
			if (!__search_userid (opts, argv[0], &buffer))
				fprintf (stderr, "not found\n");
			break;
	}
	_curl_buffer_free (&buffer, true);
	return NULL;
}

static void *
_id (struct curl_opts_t *opts, void *cont_data, uint8_t id, size_t argc, char **argv)
{
	struct _curl_buffer_t buffer;
	_curl_buffer_free (&buffer, false);
	switch (id)
	{
		case _GET:
			if (!opts->u.name[0])
			{
				fprintf (stderr, "id: username not defined\n");
			}
			else
			{
				if (!opts->u.name[0])
					__search_userid (opts, opts->u.name, &buffer);
				printf ("ID: [%s, %s] %s\n",
					opts->u.flags & _USER_BLOCKED ? "Blocked" : "Active",\
					opts->u.flags & _USER_DELETED ? "Deleted" : "Exists",
					opts->u.id);
			}
			break;
		case _SET:
			/* update info */
			if (!__search_userid (opts, argv[0], &buffer))
				fprintf (stderr, "id: not found\n");
			break;
	}
	_curl_buffer_free (&buffer, true);
	return NULL;
}

static void *
_passwd (struct curl_opts_t *opts, void *cont_data, uint8_t id, size_t argc, char **argv)
{
	if (!opts->u.id[0] && !opts->u.password[0])
	{
		fprintf (stderr, "passwd: ID not found or not defined\n");
		return NULL;
	}
	if (!(opts->u.flags & _USER_DELETED))
	{
		char url[1024];
		struct _curl_buffer_t buffer;
		_curl_buffer_free (&buffer, false);
		if (id == _SET)
		{
			if (!_auth (opts, false))
				return NULL;
			/* TODO: add 'send' key for only re-send password to user */
			if (!strncmp (argv[0], "new", 4))
			{
				snprintf (url, 1024, "http://hp.beget.ru/customerinfo%s?ajax&method=ajax_generate_passwd",\
					opts->u.id);
				_curl_query (opts, NULL, &opts->cookie, url, "", 0);
				/* get password */
				__search_update_fields (opts, NULL, _UPDATE_STEP_PASSWORD, &buffer);
			}
			else
			if (!strncmp (argv[0], "sup", 4))
			{
				snprintf (url, 1024, "http://hp.beget.ru/customerinfo%s?ajax&method=ajax_send_register_date_support",\
					opts->u.id);
			}
			else
			if (!strncmp (argv[0], "reming", 6))
			{
				snprintf (url, 1024, "http://hp.beget.ru/customerinfo%s?ajax&method=ajax_send_register_date",\
					opts->u.id);
				_curl_query (opts, NULL, &opts->cookie, url, "", 0);
			}
			else
			{
				fprintf (stderr, "passwd: first argument must be in <new|reming|support>\n");
			}
		}
		if (!opts->u.password[0])
		{
			_curl_buffer_free (&buffer, true);
			__search_update_fields (opts, NULL, _UPDATE_STEP_PASSWORD, &buffer);
		}
		printf ("password: %s\n", opts->u.password);
		_curl_buffer_free (&buffer, true);
	}
	else
	{
		fprintf (stderr, "passwd: user '%s' has flags: [%s, %s]\n",
				opts->u.name,\
				opts->u.flags & _USER_BLOCKED ? "Blocked" : "Active",\
				opts->u.flags & _USER_DELETED ? "Deleted" : "Exists");
	}
	return NULL;
}

static void *
_server (struct curl_opts_t *opts, void *cont_data, uint8_t id, size_t argc, char **argv)
{
	if (!opts->u.server[0])
	{
		fprintf (stderr, "server: ID or username not defined\n");
		return NULL;
	}
	fprintf (stderr, "Server: %s\n", opts->u.server);
	return NULL;
}

static void *
_ssh (struct curl_opts_t *opts, void *cont_data, uint8_t id, size_t argc, char **argv)
{
	struct _curl_buffer_t buffer;
	_curl_buffer_free (&buffer, false);
	if (!opts->u.id[0])
	{
		fprintf (stderr, "ssh: ID or username not defined\n");
		return NULL;
	}
	/* check args */
	if (!strncmp (argv[0], "open", 5))
		id = 0;
	else
	if (!strncmp (argv[0], "close", 6))
		id = 1;
	else
	if (!strncmp (argv[0], "on", 3))
		id = 2;
	else
	if (!strncmp (argv[0], "off", 4))
		id = 3;
	else
	if (!strncmp (argv[0], "store", 6))
		id = 4;
	else
	if (!strncmp (argv[0], "drop", 5))
		id = 5;
	else
	{
		return OPTS_BADARGS;
	}
	/* check flags */
	if (opts->u.flags & (_USER_DELETED | _USER_BLOCKED))
	{
		fprintf (stderr, "ssh: user '%s' has flags: [%s, %s]\n",
				opts->u.name,\
				opts->u.flags & _USER_BLOCKED ? "Blocked" : "Active",\
				opts->u.flags & _USER_DELETED ? "Deleted" : "Exists");
		return NULL;
	}
	/* check auth */
	if (!_auth (opts, false))
		return NULL;
	/* switch ssh state */
	{

		char url[1024];
		snprintf (url, 1024,\
			"http://hp.beget.ru/customerinfo%s?ajax&method=ajax_change_ssh",\
			opts->u.id);
		/* if open || on -> switch on ssh */
		if (id == 0 || id == 2)
		{
			_curl_query (opts, NULL, &opts->cookie, url, "p0=Y", 0);
		}
		else
		/* if close || off -> switch off ssh */
		if (id == 1 || id == 3)
		{
			_curl_query (opts, NULL, &opts->cookie, url, "p0=N", 0);
		}
		/* check ssh status */
		{
			char *rrgv[7]; /* 6 fields */
			size_t rrgc;
			snprintf (url, 1024, "p0=%s&p1=-&p2=-&p3=-&p4=-", opts->u.id);
			_curl_query (opts, &buffer, &opts->cookie,
				"http://hp.beget.ru/main?ajax&method=ajax_get_status",\
				url, _FLAG_DATA);
			if (buffer.data)
			{
				rrgc = _line2array (buffer.data, 7, rrgv);
				if (rrgc == 6)
					fprintf (stderr, "ssh status: %s\n", rrgv[1][0] == 'Y' ? "on" : "off");
			}
		}
	}
	/* if cmd in [on, off] then return */
	if (id != 2 && id != 3)
	{
		if (!opts->opts.sshkey_data[0])
		{
			fprintf (stderr, "ssh: sshkey not defined\n");
		}
		else
		{
			/* get password */
			if (!opts->u.password[0])
				__search_update_fields (opts, NULL, _UPDATE_STEP_PASSWORD, &buffer);
			/* remove or add ssh key */
			switch (id)
			{
				case 0: /* open */
				case 4: /* store */
					__ssh_update_keys (opts, _UPDATE_KEYADD, &buffer, opts->opts.sshkey_data, opts->opts.sshkey_size);
					printf ("%s@%s.beget.ru\n", opts->u.name, opts->u.server);
					break;
				case 1: /* close */
				case 5: /* drop */
					__ssh_update_keys (opts, _UPDATE_KEYREM, &buffer, opts->opts.sshkey_data, opts->opts.sshkey_size);
					break;
			}
		}
	}
	_curl_buffer_free (&buffer, true);
	return NULL;
}

static void *
_db (struct curl_opts_t *opts, void *cont_data, uint8_t id, size_t argc, char **argv)
{
	DBC *dbc = NULL;
	DBT key;
	DBT val;
	struct _db_store_t *dbs;
	if (!opts->opts.db)
	{
		fprintf (stderr, "db: database not open\n");
		return NULL;
	}
	/* check args */
	if (!strncmp (argv[0], "list", 5))
	{
		id = _GET;
	}
	else
	if (!strncmp (argv[0], "clear", 6))
	{
		id = _SET;
	}
	else
		return OPTS_BADARGS;
	/* */
	if (cont_data && id == _GET)
		dbc = (DBC *)cont_data;
	else
	if (id == _SET || id == _GET)
		opts->opts.db->cursor (opts->opts.db, NULL, &dbc, 0);
	memset (&key, 0, sizeof (DBT));
	memset (&val, 0, sizeof (DBT));
	/* */
	if (dbc)
	{
		if (!dbc->get (dbc, &key, &val, DB_NEXT))
		{
			char *tmp;
			dbs = val.data;
			/* copy data */
			{
				memcpy ((void *)opts->u.id, (const void *)dbs->id, _CURL_SZ);
				memcpy ((void *)opts->u.name, (const void *)dbs->user, _CURL_SZ);
				memcpy ((void *)opts->u.password, (const void *)dbs->password, _CURL_SZ);
				memcpy ((void *)opts->u.server, (const void *)dbs->server, _CURL_SZ);
			}
			if (id == _GET)
			{
				if (!cont_data)
					printf ("%8s | %12s | %10s | %s\n"\
						"----------------------------"\
						"----------------------------"
						"------------------------\n",\
						"ID", "username", "server", "ssh key");
				/* strip \n */
				if ((tmp = strchr (dbs->sshkey_data, '\n')))
					*tmp = '\0';
				printf ("%8s | %12s | %10s | %s\n",\
					dbs->id, dbs->user, dbs->server, dbs->sshkey_data);
			}
			else
			if (id == _SET)
			{
				/* remove keys from users */
				size_t len = strlen (dbs->sshkey_data);
				/* TODO: buffer must be not created in cycle */
				struct _curl_buffer_t buffer;
				_curl_buffer_free (&buffer, false);
				fprintf (stderr, "db: clear %s with key %s\n", dbs->user, dbs->sshkey_data);
				/* TODO: need more auth checks */
				__ssh_update_keys (opts, _UPDATE_KEYREM, &buffer,\
					dbs->sshkey_data, len);
				_curl_buffer_free (&buffer, true);
			}
			return dbc;
		}
		else
			dbc->close (dbc);
	}
	return NULL;
}

/*** cp funcs */
static bool
_cp_auth (struct curl_opts_t *opts, bool noupdate)
{
	long ret;
	char fields[1024];
	/* check status */
	if (opts->u.flags & (_USER_DELETED | _USER_BLOCKED))
	{
		fprintf (stderr, "cp_auth: user '%s' has flags: [%s, %s]\n",
				opts->u.name,\
				opts->u.flags & _USER_BLOCKED ? "Blocked" : "Active",\
				opts->u.flags & _USER_DELETED ? "Deleted" : "Exists");
		return false;
	}
	/* check exists cookie */
	if (opts->u.cookie)
	{
		ret = _curl_query (opts, NULL, &opts->u.cookie, "http://cp.beget.ru/info?ajaxj&method=ajaxj_direct_load_data", "", 0);
		if (ret == 200)
			return true;
	}
	/* auth */
	if (!noupdate)
	{
		snprintf (fields, 1024, "name=%s&password=%s&ssl=off&submit=", opts->u.name, opts->u.password);
		_curl_query (opts, NULL, &opts->u.cookie, "http://cp.beget.ru/login_.php", fields, _FLAG_COOK);
		if (_cp_auth (opts, true))
			return true;
		fprintf (stderr, "cp_auth: check failed\n");
	}
	/* free invalid cookies */
	if (opts->u.cookie)
	{
		free (opts->u.cookie);
		opts->u.cookie = NULL;
	}
	return false;
}

/* helps */
static void
_server_help (FILE *file, char *cmd, uint8_t id, uint8_t type)
{
	if (type & OPTS_HELP_SHORT)
			fprintf (file, "%s\n", cmd);
	if (type & OPTS_HELP_FULL)
			fprintf (file, "Отображает сервер, на котором распологается пользователь\n");
}

static void
_user_help (FILE *file, char *cmd, uint8_t id, uint8_t type)
{
	if (type & OPTS_HELP_SHORT)
		fprintf (file, "%s [username]\n", cmd);
	if (type & OPTS_HELP_FULL)
		fprintf (file, "Назначает или выводит username\n");
}

static void
_id_help (FILE *file, char *cmd, uint8_t id, uint8_t type)
{
	if (type & OPTS_HELP_SHORT)
		fprintf (file, "%s [id]\n", cmd);
	if (type & OPTS_HELP_FULL)
		fprintf (file, "Назначает или выводит id пользователя\n");
}

static void
_search_help (FILE *file, char *cmd, uint8_t id, uint8_t type)
{
	if (type & OPTS_HELP_SHORT)
			fprintf (file, "%s <all|email|domain|vat> needla [<eq|ap>]\n", cmd);
	if (type & OPTS_HELP_FULL)
	{
		fprintf (file, "поиск\n"\
						"<all|email|domain|vat>: поиск по <любому|почтому адресу|домену|ИНН>\n"\
						"needle: поисковая строка\n"\
						"[<eq|ap>]: необязательный, поиск с <полное совпадение|вхождение>, если не назначено -- поиск по вхождению\n");
		fprintf (file, "! данная команда позволяет себе вызывать все последующие комманды для каждой выведенной строки\n");
		fprintf (file, "Пример:\n"\
						" search all beget\n"\
						" search domain bmcms.ru eq"\
						" search email support@beget.ru \\; passwd sup\n"\
						"  # вышлет email на support@beget.ru для _всех_ пользователей с email support@beget.ru, кроме удалённых\n");
	}
}

static void
_ssh_help (FILE *file, char *cmd, uint8_t id, uint8_t type)
{
	if (type & OPTS_HELP_SHORT)
		fprintf (file, "%s <on|store|open|off|drop|close>\n", cmd);
	if (type & OPTS_HELP_FULL)
	{
		fprintf (file, "on: включает ssh-доступ\n"\
			"store: кладёт ключ в ~/.ssh/authorized_keys по ftp пользователю\n"\
			"open: комбинация on и store\n"\
			"off: выключает ssh-доступ\n"\
			"drop: вырезает ключ из ~/.ssh/authorized_keys по ftp пользователя\n"\
			"close: комбинация on и drop\n");
	}
}

static void
_db_help (FILE *file, char *cmd, uint8_t id, uint8_t type)
{
	if (type & OPTS_HELP_SHORT)
			fprintf (file, "%s <list|clear>\n", cmd);
	if (type & OPTS_HELP_FULL)
			fprintf (file, "Управление внутренним списком"\
						"list: выводит установленных пользователям ключей\n"\
						"clear: чистит список и пытается удалить установленные ключи у пользователей\n");
}

static void
_passwd_help (FILE *file, char *cmd, uint8_t id, uint8_t type)
{
	if (type & OPTS_HELP_SHORT)
		fprintf (file, "%s [<new|reming|sup>]\n", cmd);
	if (type & OPTS_HELP_FULL)
	{
		fprintf (file, "Отображает пароль пользователя и выполняет:\n"\
						"new: генерирует новый пароль и высылает email пользователю\n"\
						"reming: высылает новый пароль пользователю\n"\
						"sup: высылает пароль на support@beget.ru\n");
		fprintf (file, "! на удалённые аккаунты (с тарифным планом \"Deleted\" или \"-\") отправка/генерация не осуществляется\n"\
						"Пример:\n"\
						"id test \\; passwd\n"\
						"user testtomas \\; passwd new\n"\
						"search all test \\; passwd sup\n");
	}
}

/* struct list */
struct opts_list_t curl_list[] =
{
	{"server", _GET, 0, (opts_callback_t)_server, 0,_server_help},
	{"user", _GET, 0, (opts_callback_t)_user, 0, _user_help},
	{"user", _SET, OPTS_NOHELP, (opts_callback_t) _user, 1, _user_help},
	{"id", _GET, 0, (opts_callback_t)_id, 0, _id_help},
	{"id", _SET, OPTS_NOHELP, (opts_callback_t)_id, 1, _id_help},
	{"search", 0, 0,(opts_callback_t)_search, 2, _search_help},
	{"search", 0, OPTS_NOHELP, (opts_callback_t)_search, 3, _search_help},
	{"passwd", _GET, 0, (opts_callback_t)_passwd, 0, _passwd_help},
	{"passwd", _SET, OPTS_NOHELP, (opts_callback_t)_passwd, 1, _passwd_help},
	{"ssh", 0, 0, (opts_callback_t)_ssh, 1, _ssh_help},
	{"db", 0, 0, (opts_callback_t)_db, 1, _db_help},
	{"", 0, 0, NULL, 0, NULL}
};

