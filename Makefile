# vim: ft=make ff=unix fenc=utf-8
# file: Makefile
# gcc -g main.c opts.c opts_func.c curl_func.c -lcurl -ldb
LIBS=-lcurl -ldb
CFLAGS=-Wall -pedantic -ggdb
BIN=./bssh
SRC=main.c opts.c opts_func.c curl_func.c

all: ${BIN}
	${BIN}

${BIN}: ${SRC}
	${CC} -o ${BIN} ${CFLAGS} ${SRC} ${LIBS}

