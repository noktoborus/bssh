/* vim: ft=c ff=unix fenc=utf-8
 * file: curl_func.h
 */
#ifndef _CURL_FUNC_1363518555_H_
#define _CURL_FUNC_1363518555_H_
#include "opts.h"
#include "opts_func.h"

#define _GET 0
#define _SET 1

#define _CURL_SZ 36
#define _USER_BLOCKED 1
#define _USER_DELETED 2
struct curl_opts_t
{
	struct opts_t opts;
	struct
	{
		char name[_CURL_SZ];
		char password[_CURL_SZ];
		char id[_CURL_SZ];
		char server[_CURL_SZ];
		char *cookie;
		uint8_t flags;
	} u;
	/* */
	char *cookie;
};

struct _curl_buffer_t
{
	char *data;
	size_t sz; /* size */
	size_t up;
};

struct _db_store_t
{
	char id[_CURL_SZ];
	char user[_CURL_SZ];
	char password[_CURL_SZ];
	char server[_CURL_SZ];
	char sshkey_data[_OPTS_MAXSSHKEY];
	uint8_t flags;
};

#define _FLAG_COOK  1 /* get cookie */
#define _FLAG_DATA  4 /* get data */
#define _FLAG_VERB  8 /* curl verbose */

#define _UPDATE_KEYADD  1
#define _UPDATE_KEYREM  2
#define _UPDATE_KEYPERM 4
#define _UPDATE_KEYNONE 8

#define _UPDATE_STEP_CLEAR      1
#define _UPDATE_STEP_COPYFIELDS 2
#define _UPDATE_STEP_PASSWORD   4

extern struct opts_list_t curl_list[];

void curlf_init (struct curl_opts_t *opts);
void curlf_term (struct curl_opts_t *opts);

/* if m == true then free (buffer.data) && memset (0)
 * else then only memset
 */
void _curl_buffer_free (struct _curl_buffer_t *buffer, bool f);

#endif /* _CURL_FUNC_1363518555_H_ */

