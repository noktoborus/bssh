/* vim: ft=c ff=unix fenc=utf-8
 * file: mail.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <curl/curl.h>
#include "opts.h"
#include "opts_func.h"
#include "curl_func.h"

int
main (int argc, char *argv[])
{
	struct curl_opts_t opts;
	char path[MAXPATH + 1];
	char *env;
	memset ((void*)&opts, 0, sizeof (struct curl_opts_t));
	/* parse configs */
	env = getenv ("HOME");
	if (env)
	{
		snprintf (path, MAXPATH, "%s/.beget.cfg", env);
		opts_parse_file (&opts.opts, opts_list, path);
	}
	opts_parse_file (&opts.opts, opts_list, ".beget.cfg");
	/* parse args */
	curlf_init (&opts);
	opts_parse (&opts.opts, curl_list, (size_t)(argc - 1), argv + 1);
	curlf_term (&opts);
	return EXIT_SUCCESS;
}

